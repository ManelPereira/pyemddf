
pyemddf
===========

EMD-DF is a sound file format developed in java.
---
Example notebook inside the [pyemddf/docs](https://gitlab.com/ManelPereira/pyemddf/-/tree/master/pyemddf/docs) folder

Install it from [PyPI](https://pypi.org/project/pyemddf/) by using **pip install pyemddf** 

Core project available at: [EMD-DF](https://gitlab.com/alspereira/EMD-DF)
